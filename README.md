# localstack-kms-region-issue

This repository is a minimal reproducible example of an issue in [data-cryptography-java](https://gitlab.com/dwp/health/shared-components/libraries/data-cryptography-java) where the AWS region is not correctly defined when creating endpoint configuration.

## Steps to reproduce

1. Open a Terminal session at the root of the repository.
1. Run `mvn clean install -DskipTests`.
1. Run `docker-compose up` and wait for the Spring app to initialise.
1. In a separate Terminal session, run `curl -H "Content-Type: application/json" -d '{ "firstName": "Joe" }' http://localhost:8080/customers` (you should see a 500 error).
1. Check the output from `docker-compose`. You should see a `KeyError` (e.g. `localstack_1 | KeyError: 'EU_WEST_2'`).

## Diagnosis

This error is caused by an in correct `Authorization` header being sent to KMS. Specifically, it is the region which is incorrect in the `Credential` attribute:

```
headers: {
  'Remote-Addr': '172.19.0.5',
  'Host': 'localstack:4566', 
  'Amz-Sdk-Invocation-Id': '28d1bea2-1359-9ef1-f4f7-ba12fac6ab08',
  'Amz-Sdk-Request': 'ttl=20210831T112019Z;attempt=4;max=4',
  'Amz-Sdk-Retry': '3/70/485',
  'Authorization': 'AWS4-HMAC-SHA256 Credential=test/20210831/EU_WEST_2/kms/aws4_request, SignedHeaders=amz-sdk-invocation-id;amz-sdk-request;amz-sdk-retry;content-length;content-type;host;user-agent;x-amz-date;x-amz-target, Signature=8dd7c260111d1258c889b4a7c180add12920faa41db2033b307cab8d29127777',
  'Content-Type': 'application/x-amz-json-1.1',
  'User-Agent': 'aws-sdk-java/1.12.56 Linux/5.10.25-linuxkit OpenJDK_64-Bit_Server_VM/11.0.12+7-post-Debian-2deb10u1 java/11.0.12 vendor/Debian cfg/retry-mode/legacy',
  'X-Amz-Date': '20210831T111929Z',
  'X-Amz-Target': 'TrentService.GenerateDataKey',
  'Content-Length': '54',
  'Connection': 'Keep-Alive',
  'X-Forwarded-For': '172.19.0.5, localstack:4566',
  'x-localstack-edge': 'http://localstack:4566'
}
```

The request is incorrect because the endpoint configuration receives an incorrect value for the region parameter (see `src/main/java/com.example.kmsapp/CustomerController.java`):

```
var kmsRegion = Regions.valueOf(cryptoConfig.getRegion()).toString();
logger.info("Region: " + kmsRegion);
var kms = AWSKMSClientBuilder.standard()
        .withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration(
                        cryptoConfig.getKmsEndpointOverride(),
                        kmsRegion))
        .withCredentials(new DefaultAWSCredentialsProviderChain())
        .build();
```

This problem can be fixed by changing the assignment of `kmsRegion` to `Regions.valueOf(cryptoConfig.getRegion()).getName();`.
