FROM gcr.io/distroless/java:11@sha256:d037e7d163de48d0be1bcbfb4b864295ce78d7f09d4105a13dba441053409528
COPY target/localstack-kms-region-issue-*.jar /localstack-kms-region-issue.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/localstack-kms-region-issue.jar"]
