#!/usr/bin/env bash

export AWS_ACCESS_KEY_ID=test
export AWS_SECRET_ACCESS_KEY=test
export AWS_REGION=eu-west-2
export AWS_DEFAULT_FORMAT=json

create_kms_key() {
  keyId=$(/usr/local/bin/aws kms create-key --endpoint-url http://localstack:4566 | awk '/"KeyId":/ { print }' | sed -r 's/.*:\s"?(.*)".*/\1/')
  echo "created KMS key '$keyId' for alias '$1'"
  /usr/local/bin/aws kms create-alias --endpoint-url http://localstack:4566 --alias-name "$1" --target-key-id "$keyId"
}

create_kms_key "alias/test_data_key_id"

tail -f /dev/null
