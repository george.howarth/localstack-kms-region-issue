package com.example.kmsapp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("application.encryption")
public class CryptoConfig {

    private String dataKeyId;
    private String kmsEndpointOverride;
    private String region;

    public String getDataKeyId() {
        return dataKeyId;
    }

    public void setDataKeyId(String dataKeyId) {
        this.dataKeyId = dataKeyId;
    }

    public String getKmsEndpointOverride() {
        return kmsEndpointOverride;
    }

    public void setKmsEndpointOverride(String kmsEndpointOverride) {
        this.kmsEndpointOverride = kmsEndpointOverride;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
