package com.example.kmsapp;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kms.AWSKMSClientBuilder;
import com.amazonaws.services.kms.model.DataKeySpec;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.services.kms.model.GenerateDataKeyRequest;
import org.bouncycastle.jcajce.provider.BouncyCastleFipsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Base64;
import java.util.Map;

@RestController
@RequestMapping(path = "/customers",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerController {

    Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CryptoConfig cryptoConfig;

    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping
    public ResponseEntity<Customer> getByFirstName(@RequestParam("firstName") String firstName) {
        var customer = customerRepository.findByFirstName(firstName);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Customer> create(@RequestBody Map<String, String> values) {
        var encryptedFirstName = encrypt(values.get("firstName"));
        var customer = new Customer(encryptedFirstName);
        var c = customerRepository.save(customer);
        return new ResponseEntity<>(c, HttpStatus.CREATED);
    }

    private String encrypt(String value) {
        // call getName instead of toString to fix the issue
        var kmsRegion = Regions.valueOf(cryptoConfig.getRegion()).toString();
        logger.info("Region: " + kmsRegion);
        var kms = AWSKMSClientBuilder.standard()
                .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration(
                                cryptoConfig.getKmsEndpointOverride(),
                                kmsRegion))
                .withCredentials(new DefaultAWSCredentialsProviderChain())
                .build();

        var dataKeyRequest = new GenerateDataKeyRequest();
        dataKeyRequest.setKeyId(cryptoConfig.getDataKeyId());
        dataKeyRequest.setKeySpec(DataKeySpec.AES_256);
        var dataKeyResult = kms.generateDataKey(dataKeyRequest);
        var ciphertextBuffer = dataKeyResult.getCiphertextBlob().duplicate();
        var key = Base64.getEncoder().encodeToString(ciphertextBuffer.array());

        DecryptRequest decryptRequest = new DecryptRequest();
        decryptRequest.setCiphertextBlob(
                ByteBuffer.wrap(
                        Base64.getDecoder().decode(
                                key.getBytes(StandardCharsets.ISO_8859_1))));
        var keyBuffer = kms.decrypt(decryptRequest).getPlaintext().duplicate();
        var keyByteArray = new byte[keyBuffer.remaining()];
        keyBuffer.get(keyByteArray);
        var secretKey = new SecretKeySpec(keyByteArray, "AES");

        var ivLength = 16;
        var iv = new byte[ivLength];

        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);

        var gcmParameterSpec = new GCMParameterSpec(128, iv, 0, ivLength);

        ByteArrayOutputStream outputStream;

        var provider = new BouncyCastleFipsProvider();
        Security.addProvider(provider);

        try {
            var cipher = Cipher.getInstance("AES/GCM/NoPadding", provider.getName());
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, gcmParameterSpec);
            var encryptedValue = cipher.doFinal(value.getBytes(StandardCharsets.UTF_8));

            outputStream = new ByteArrayOutputStream();
            outputStream.write(ivLength);
            outputStream.write(iv);
            outputStream.write(encryptedValue);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return "";
        }

        return Base64.getEncoder().encodeToString(outputStream.toByteArray());
    }
}
