package com.example.kmsapp;

import org.springframework.data.annotation.Id;

public class Customer {

    @Id
    public String id;

    public String firstName;

    public Customer(String firstName) {
        this.firstName = firstName;
    }
}
